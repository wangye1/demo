package cn.com.testUser;

import cn.com.dao.CompanyMemberMapper;
import cn.com.entity.CompanyMember;
import cn.com.util.CompanyMemberUtil;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.Reader;
import java.util.List;

/**
 * Created by wangye on 2016/11/30.
 */
public class MybatisTest {
    private Reader reader;
    private SqlSessionFactory sqlSessionFactory;
    SqlSession session;

    @Before
    public void before() {
        try {
            reader = Resources.getResourceAsReader("mybatis.xml");
        } catch (IOException e) {
            System.out.print("读取配置文件异常");
            return;
        }
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
    }


    @After
    public void fater() {
        if (session != null) {
            session.close();
            System.out.print("session关闭成功");
        }
    }

    @Test
    public void test() {
        try {
            session = sqlSessionFactory.openSession();
            CompanyMemberMapper companyMemberMapper = session.getMapper(CompanyMemberMapper.class);
            CompanyMember companyMember = new CompanyMember();
            companyMember.setUserName("王叶");
            companyMember.setMobile("18969055453");
            companyMember.setUserId(126);
            //mapper映射文件插入
//            session.insert("cn.com.dao.CompanyMemberMapper.insert",companyMember);
            companyMemberMapper.insert(companyMember);
        } catch (Exception e) {
            System.out.print("打开session异常");
            e.printStackTrace();
        }
    }

    @Test
    public void testlist() {
        try {
            session = sqlSessionFactory.openSession();
            CompanyMemberMapper companyMemberMapper = session.getMapper(CompanyMemberMapper.class);
            List<CompanyMember> list = CompanyMemberUtil.getAllCompanyMemberFormExcel("C:\\Users\\wangye\\Desktop\\fn_company_member.xls");
            System.out.println(list.size());
            int i = companyMemberMapper.saveCompanyMemberList(list);
            System.out.println("影响行数:" + i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

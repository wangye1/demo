package cn.com.entity;

import java.io.Serializable;
import java.util.Date;

public class CompanyMember implements Serializable {
    private Integer userId;

    private String userCode;

    private Integer companyId;

    private String userName;

    private String imei;

    private String passwd;

    private Integer userType;

    private Integer status;

    private Integer onlineFlag;

    private Date createDatetime;

    private String permissions;

    private Integer level;

    private Integer defaultGroupId;

    private String groupIds;

    private String mobile;

    private Integer sex;

    private String version;

    private Integer currGroupId;

    private String currGroupName;

//    private GeoItem geoItem;

    private static final long serialVersionUID = 1L;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserCode() { return userCode;}

    public void setUserCode(String userCode) { this.userCode = userCode;}

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd =passwd ;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getOnlineFlag() {
        return onlineFlag;
    }

    public void setOnlineFlag(Integer onlineFlag) {
        this.onlineFlag = onlineFlag;
    }

    public Date getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getDefaultGroupId() {
        return defaultGroupId;
    }

    public void setDefaultGroupId(Integer defaultGroupId) {
        this.defaultGroupId = defaultGroupId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getGroupIds() {
        return groupIds;
    }

    public void setGroupIds(String groupIds) {
        this.groupIds = groupIds;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Integer getCurrGroupId() {
        return currGroupId;
    }

    public void setCurrGroupId(Integer currGroupId) {
        this.currGroupId = currGroupId;
    }

//    public GeoItem getGeoItem() {
//        return geoItem;
//    }
//
//    public void setGeoItem(GeoItem geoItem) {
//        this.geoItem = geoItem;
//    }

    public String getCurrGroupName() {
        return currGroupName;
    }

    public void setCurrGroupName(String currGroupName) {
        this.currGroupName = currGroupName;
    }

    @Override
    public String toString() {
        StringBuilder giStr = new StringBuilder();
//        GeoItem gi = getGeoItem();
//        if(gi != null){
//            giStr.append(gi.toString());
//        }else{
//            giStr.append("{}");
//        }
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"Hash\":").append(hashCode());
        sb.append(", \"userId\":").append(getUserId());
        sb.append(", \"userCode\":").append("\"" + getUserCode() + "\"");
        sb.append(", \"companyId\":").append(getCompanyId());
        sb.append(", \"userName\":").append("\"" + getUserName() + "\"");
        sb.append(", \"imei\":").append("\"" + getImei() + "\"");
        sb.append(", \"userType\":").append(getUserType());
        sb.append(", \"status\":").append(getStatus());
        sb.append(", \"onlineFlag\":").append(getOnlineFlag());
//        sb.append(", \"createDatetime\":").append("\"" + DateUtil.dateTime2String(getCreateDatetime()) + "\"");
//        sb.append(", \"permissions\":").append("\"" + getPermissions() + "\"");
        sb.append(", \"level\":").append(getLevel());
        sb.append(", \"defaultGroupId\":").append(getDefaultGroupId());
        sb.append(", \"currGroupId\":").append(getCurrGroupId());
        sb.append(", \"currGroupName\":").append("\"" + getCurrGroupName() + "\"");
        sb.append(", \"mobile\":").append("\"" + getMobile() + "\"");
        sb.append(", \"sex\":").append(getSex());
        sb.append(", \"version\":").append("\"" + getVersion() + "\"");
        sb.append(", \"position\":" + giStr.toString() + "");
        sb.append("}");
        return sb.toString();
    }
}
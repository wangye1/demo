package cn.com.service.impl;

import cn.com.dao.CompanyMemberMapper;
import cn.com.entity.CompanyMember;
import cn.com.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by wangye on 2016/12/1.
 */
@Service("companyService")
public class CompanyServiceImpl implements CompanyService {
    @Autowired
    CompanyMemberMapper companyMemberMapper;

    @Override
    public int saveCompanyMemberList(List<CompanyMember> list){
        int i;
        try {
            i = companyMemberMapper.saveCompanyMemberList(list);
        }catch (Exception e){
            i=0;
            System.out.print("服务层：CompanyServiceImpl. saveCompanyMemberList(List<CompanyMember> list)执行异常。"+e.getMessage());
            e.printStackTrace();
        }
        return i;
    }
}

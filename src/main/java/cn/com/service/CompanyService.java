package cn.com.service;

import cn.com.entity.CompanyMember;

import java.util.List;

/**
 * Created by wangye on 2016/11/30.
 */
public interface CompanyService {
    /**
     * 批量插入公司用户  CompanyMember
     * @param list CompanyMemberList
     * @return
     */
    int saveCompanyMemberList(List<CompanyMember> list);
}

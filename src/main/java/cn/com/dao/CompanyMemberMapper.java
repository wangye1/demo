package cn.com.dao;

import cn.com.entity.CompanyMember;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyMemberMapper {
    int insert(CompanyMember record);
    /**
     * 批量插入公司用户  CompanyMember
     * @param list CompanyMemberList
     * @return
     */
    int saveCompanyMemberList(List<CompanyMember> list);
}
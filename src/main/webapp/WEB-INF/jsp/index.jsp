<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">
    <title>index</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
    <script src="scripts/jquery-1.7.1.min.js"></script>
    <style type="text/css">
        #orderform .mini-textbox {
            width: 250px;
        }

       td {
            border:solid #add9c0 1px;
        }
        table{
            border-collapse:collapse;
        }
    </style>
</head>
<body class="bodycolor1">
<%--<div id="content">${companyMemberList}</div>--%>

<div>
    <table id="tb">
        <tr>
            <th>用户名称</th>
            <th>用户类别</th>
            <th>性别</th>
            <th>IMMI号</th>
            <th>手机号</th>
            <th>状态</th>
            <th>群组</th>
            <th>默认群组</th>
            <th>权限</th>
        </tr>
        <c:forEach items="${companyMemberList}" var="companyMember">
            <tr>
                <td>${companyMember.userName}</td>
                <td>${companyMember.userType}</td>
                <td>${companyMember.sex}</td>
                <td>${companyMember.imei}</td>
                <td>${companyMember.mobile}</td>
                <td>${companyMember.status}</td>
                <td>${companyMember.groupIds}</td>
                <td>${companyMember.defaultGroupId}</td>
                <td>${companyMember.permissions}</td>
            </tr>
        </c:forEach>
    </table>
</div>

<script>
    function query() {
        var data = {"id": "1"};
        $.ajax({
            url: "/jxc/manager/showUser",
            type: "post",
            data: data,
            contentType: "application/json",
            dataType: "json",
            success: function (result) {
                alert("成功");
            },
            error: function (result) {
                alert("失败");
            }
        })
    }
</script>
</body>
</html>